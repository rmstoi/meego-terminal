TEMPLATE        = app
DESTDIR         = ..

CONFIG          += qt debug_and_release warn_on # build_all
CONFIG          += meegotouch

QT += core gui

MOC_DIR         = ../.moc
LIBS            += -L.. -lX11 -lmeegotouchviews

CONFIG(debug, debug|release) {
    OBJECTS_DIR = ../.objs_d
    TARGET      = meego-terminal_d
    LIBS        += -lqgraphicstermwidget_d
    PRE_TARGETDEPS += ../libqgraphicstermwidget_d.a
} else {
    OBJECTS_DIR = ../.objs
    TARGET      = meego-terminal
    LIBS        += -lqgraphicstermwidget
    PRE_TARGETDEPS += ../libqgraphicstermwidget.a
}

LMTP_HEADERS    = lmtp/mtopleveloverlay.h lmtp/meditortoolbararrow.h \
                  lmtp/meditortoolbar_p.h lmtp/meditortoolbar.h
LMTP_SOURCES    = lmtp/mtopleveloverlay.cpp lmtp/meditortoolbararrow.cpp \
                  lmtp/meditortoolbar.cpp

HEADERS         = MTermWidget.h MTerminalDisplay.h $$LMTP_HEADERS
SOURCES         = main.cpp MTermWidget.cpp MTerminalDisplay.cpp $$LMTP_SOURCES

INCLUDEPATH     = ../lib ./lmtp

#LIBS           += -L.. -lqgraphicstermwidget

desktop_entry.path = $$[QT_INSTALL_PREFIX]/share/applications
desktop_entry.files = meego-terminal.desktop

imtoolbars.path = $$[QT_INSTALL_PREFIX]/share/meego-terminal/toolbars
imtoolbars.files = toolbars/shell.xml toolbars/arrows.xml

target.path = $$[QT_INSTALL_PREFIX]/bin

INSTALLS        += target desktop_entry imtoolbars
