/*
    This file is part of meego-terminal

    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).

    Contact: Ruslan Mstoi <ruslan.mstoi@nokia.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.
*/

#include <MPannableViewport>
#include <MInputMethodState>
#include <MPositionIndicator>
#include <MMessageBox>
#include <MBanner>
#include <MButton>
#include <MApplication>
#include <MAction>
#include <MApplicationWindow>
#include <MApplicationPage>
#include <MWidgetAction>
#include <MComboBox>
#include <MLabel>

#include "MTermWidget.h"
#include "MTerminalDisplay.h"
#include "Session.h"
#include "ScreenWindow.h"

// multiplier that converts display lines into viewport values, purpose is to
// make panning easier to use (i.e. not too sensitive)
#define PANM 10 

/**
 * [1] Display cannot be set as viewports widget because it handles history
 * itself, so it does not render all of its contents as viewport expects. Also,
 * viewport does not support abstract ranges but range is in actual
 * coordinates. So, fake widget is used to trick the viewport to handle the pan
 * gestures. This class does the conversion from viewport coordinates into
 * terminal lines.
 */
MTermWidget::MTermWidget(int startnow, QGraphicsWidget *parent):
    QGraphicsTermWidget(parent), // do custom session and display creation
    m_lastSetCurrentLine(0),
    m_colorScheme(2), // green on black
    m_colorSchemeComboBox(0),
    m_menuButton(0),
    m_selectionButton(0),
    m_fullScreenAction(0),
    m_toolbarsComboBox(0),
    m_display(0)
{
    construct(startnow);
    m_display = dynamic_cast<MTerminalDisplay*>(m_terminalDisplay);
    Q_ASSERT(m_display);

    setupMenu();

    setAutoFillBackground(true); // see MTermWidget::setColorScheme [1]

    m_viewport = new MPannableViewport(this);
    m_viewport->setObjectName("m_viewport");
    m_viewport->setAutoRange(false);

    // note fake widget, see [1]
    QGraphicsWidget *w = new QGraphicsWidget(this);
    w->setObjectName("WV");
    w->setFlag(QGraphicsItem::ItemHasNoContents, true);
    m_viewport->setWidget(w);

    connect(m_terminalDisplay, SIGNAL(scrollChanged(int, int)),
            this, SLOT(updateViewport(int, int)));

    connect(m_viewport, SIGNAL(positionChanged(const QPointF &)),
            this, SLOT(viewportPositionChanged(const QPointF &)));

    connect(MInputMethodState::instance(),
            SIGNAL(inputMethodAreaChanged(QRect)),
            this, SLOT(onInputMethodAreaChanged(QRect)),
            Qt::UniqueConnection);

    connect(m_terminalDisplay, SIGNAL(changedFontMetricSignal(int, int)),
            this, SLOT(displayFontChanged()));

    enableGestures();
    readSettings();
}

/**
 * Creates session and display and does initialization. Basically does the same
 * as the non-default base constructor, but creates MTerminalDisplay instead of
 * TerminalDisplay.
 */
void MTermWidget::construct(int startnow)
{
    m_session = createSession();
    m_terminalDisplay = createTerminalDisplay(m_session);

    init();

    if (startnow && m_session) {
        m_session->run();
    }

    // set focus policy so that display gets focus, default focus policy is
    // Qt::NoFocus which means ItemIsFocusable flag is not set
     this->setFocusPolicy( m_terminalDisplay->focusPolicy() ); 

    this->setFocus( Qt::OtherFocusReason );
    m_terminalDisplay->resize(this->size());
    
    this->setFocusProxy(m_terminalDisplay);
}

void MTermWidget::setupMenu()
{
    MApplicationWindow *window =
        dynamic_cast<MApplicationWindow*>(MApplication::activeWindow());
    if (!window) {
        qFatal("No window!");
        return;
    }
    MApplicationPage *page = window->currentPage();
    if (!page) {
        qFatal("No page!");
        return;
    }

    MAction::Locations location = MAction::ApplicationMenuLocation;

    MAction *action = 0;
    action = new MAction("New Window", this);
    action->setLocation(location);
    page->addAction(action);
    connect(action, SIGNAL(triggered()), SLOT(createNewWindow()));

    m_toolbarsComboBox = new MComboBox(this);
    foreach(MTerminalDisplay::Toolbar* toolbar, m_display->toolbars())
        m_toolbarsComboBox->addItem(toolbar->name);
    m_toolbarsComboBox->setIconVisible(false);
    m_toolbarsComboBox->setTitle("Toolbar");
    connect(m_toolbarsComboBox, SIGNAL(currentIndexChanged(int)),
            m_display, SLOT(setActiveToolbarIndex(int)));

    MWidgetAction *widgetAction = new MWidgetAction(this);
    widgetAction->setLocation(location);
    widgetAction->setWidget(m_toolbarsComboBox);
    page->addAction(widgetAction);

    m_colorSchemeComboBox = new MComboBox(this);
    for (int i = COLOR_SCHEME_FIRST; i <= COLOR_SCHEME_LAST; ++i)
        m_colorSchemeComboBox->addItem(COLOR_SCHEME_NAME[i]);
    m_colorSchemeComboBox->setIconVisible(false);
    m_colorSchemeComboBox->setTitle("Color Scheme");
    connect(m_colorSchemeComboBox, SIGNAL(currentIndexChanged(int)),
            SLOT(onColorSchemeCbxCurrentIndexChanged(int)));

    widgetAction = new MWidgetAction(this);
    widgetAction->setLocation(location);
    widgetAction->setWidget(m_colorSchemeComboBox);
    page->addAction(widgetAction);

    m_fullScreenAction = new MAction("FullScreen", this);
    m_fullScreenAction->setLocation(location);
    m_fullScreenAction->setCheckable(true);
    connect(m_fullScreenAction, SIGNAL(toggled(bool)),
            SLOT(toggleFullScreenMode(bool)));
    page->addAction(m_fullScreenAction);

    const qreal buttonOpacity = 0.2f;

    m_menuButton = new MButton(this);
    m_menuButton->setObjectName("m_menuButton");
    m_menuButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_menuButton->setViewType(MButton::iconType);
    m_menuButton->setOpacity(buttonOpacity);
    connect(m_menuButton, SIGNAL(clicked()), window, SLOT(openMenu()));

    m_selectionButton = new MButton(this);
    m_selectionButton->setObjectName("m_selectionButton");
    m_selectionButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_selectionButton->setViewType(MButton::iconType);
    m_selectionButton->setOpacity(buttonOpacity);
    m_selectionButton->setCheckable(true);
    m_selectionButton->setChecked(false);
    connect(m_selectionButton, SIGNAL(clicked(bool)),
            SLOT(toggleSelectionModeWithBanner(bool)));

    action = new MAction("Help", this);
    action->setLocation(location);
    page->addAction(action);
    connect(action, SIGNAL(triggered()), SLOT(showHelp()));

    QGraphicsWidget *w = new QGraphicsWidget(this);
    w->setObjectName("LHW");
    w->setFlag(QGraphicsItem::ItemHasNoContents, true);
    w->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QGraphicsLinearLayout* layout =
        new QGraphicsLinearLayout(m_terminalDisplay);
    QGraphicsLinearLayout* buttonsLayout =
        new QGraphicsLinearLayout(Qt::Vertical, layout);
    buttonsLayout->setSpacing(0.0f);
    buttonsLayout->addItem(m_menuButton);
    buttonsLayout->addItem(m_selectionButton);
    layout->addItem(w);
    layout->addItem(buttonsLayout);

    // hide it not to waste space, it is hidden behind vkb anyway
    page->setComponentsDisplayMode(MApplicationPage::NavigationBar,
                                   MApplicationPageModel::Hide);
}

MTermWidget::~MTermWidget()
{
    writeSettings(); // could do it also in sessionFinished
}

TerminalDisplay *MTermWidget::createTerminalDisplay(Session *session)
{
    TerminalDisplay* display = new MTerminalDisplay(this);
    display->setObjectName("MTerminalDisplay");

    display->setBellMode(TerminalDisplay::NotifyBell);
    display->setTerminalSizeHint(true);
    display->setTripleClickMode(TerminalDisplay::SelectWholeLine);
    display->setTerminalSizeStartup(true);

    display->setRandomSeed(session->sessionId() * 31);
    
    return display;
}

void MTermWidget::resizeEvent(QGraphicsSceneResizeEvent *event)
{
    QRect imArea = MInputMethodState::instance()->inputMethodArea();

    if (imArea.isEmpty()) { // vkb is not up
        QGraphicsTermWidget::resizeEvent(event);
        m_viewport->resize(this->size());
    }
    // vkb is up
    else {
        int newHeight = 0; // = where vkb starts - status bar
        
        if (imArea.x() == 0) { // landscape
            newHeight = imArea.y() - scenePos().y();
        }

        else if (imArea.y() == 0) { // portrait
            newHeight = imArea.x() - scenePos().x();
        }

        if (newHeight > 0) {  // resize to occupy area not taken  by vkb
            m_terminalDisplay->resize(size().width(), newHeight);
            m_viewport->resize(size().width(), newHeight);
        }
    }
}

/**
 * At least when coming to portrait mode this method is called after
 * resizeEvent, so trigger resizeEvent. Parameter is null, it is not used
 * anyway.
 */
void MTermWidget::onInputMethodAreaChanged(const QRect &imArea)
{
    Q_UNUSED(imArea);
    resizeEvent(0);
}

void MTermWidget::updateViewport(int currentLine, int maxLines)
{
    QRectF newRange = m_viewport->range();
    newRange.setHeight(maxLines * PANM);

    QPointF newPos(m_viewport->position().x(), currentLine * PANM);

    // avoid rounding error when called due to viewportPositionChanged
    if (m_lastSetCurrentLine == currentLine && maxLines) {
        newPos.setY(m_viewport->position().y());
    }

    if (newRange != m_viewport->range()) {
        m_viewport->setRange(newRange);
    }

    if (newPos != m_viewport->position()) {
        m_viewport->setPosition(newPos);
    }

    static bool autoSelectionEnabled = false;
    static bool userDisabledAutoSelection = false;

    // user disabled auto-enabled selection mode, maybe to pinch, or swipe...
    if (autoSelectionEnabled && !m_selectionButton->isChecked()) {
        userDisabledAutoSelection = true;
    }
    if (userDisabledAutoSelection)
        return;

    // disable selection mode if there is history and was auto-enabled
    if (maxLines && m_selectionButton->isChecked() && autoSelectionEnabled) {
        autoSelectionEnabled = false;
        toggleSelectionMode(false);
    }

    // auto-enable selection mode if there is no history
    else if (!maxLines && !m_selectionButton->isChecked() &&
             !autoSelectionEnabled) {
        autoSelectionEnabled = true;
        toggleSelectionMode(true);
    }
}

void MTermWidget::viewportPositionChanged(const QPointF &position)
{
    static qreal prevPosY = 0;
    qreal posY = position.y();

    if (posY <  0) {// spring action this
        posY = 0;
    }

    if (posY > m_viewport->range().height()) {// spring action this
        posY = m_viewport->range().height();
    }
    
    if (prevPosY == posY) {
        return;
    }

    prevPosY = posY;
    m_lastSetCurrentLine = posY / PANM;

    const bool atEndOfOutput = (posY == m_viewport->range().height());
    m_terminalDisplay->viewportPositionChanged(m_lastSetCurrentLine,
                                               atEndOfOutput);
}

bool MTermWidget::event(QEvent *event)
{
    if (event->type() == QEvent::Gesture)
        return gestureEvent(static_cast<QGestureEvent*>(event));
    return QGraphicsTermWidget::event(event);
}

bool MTermWidget::gestureEvent(QGestureEvent *event)
{
    if (QGesture *swipe = event->gesture(Qt::SwipeGesture))
         swipeTriggered(static_cast<QSwipeGesture *>(swipe));
    if (QGesture *pinch = event->gesture(Qt::PinchGesture))
        pinchTriggered(static_cast<QPinchGesture *>(pinch));
    
    return true;
}

/**
 * The complexity in handling swipe directions is because QSwipeGesture does
 * not understand orientation change. For example, after a change from
 * landscape to portrait horizontal swipes are reported for vertical ones and
 * vice versa.
 */
void MTermWidget::swipeTriggered(QSwipeGesture *gesture)
{
    if (gesture->state() != Qt::GestureFinished) {
        return;
    }

    // defaults assume landscape orientation
    QSwipeGesture::SwipeDirection otherDirection =
        gesture->verticalDirection();
    QSwipeGesture::SwipeDirection currentDirection =
        gesture->horizontalDirection();
    QSwipeGesture::SwipeDirection prevDirection = QSwipeGesture::Left;
    QSwipeGesture::SwipeDirection nextDirection = QSwipeGesture::Right;

    // portrait (at the time of writing this QSystemDisplayInfo.orientation did
    // not work, it always returned QSystemDisplayInfo::Landscape)
    if (size().width() == 480) {
        otherDirection = gesture->horizontalDirection();
        currentDirection = gesture->verticalDirection();
        prevDirection = QSwipeGesture::Up;
        nextDirection = QSwipeGesture::Down;
    }

    // accept only clean swipes in the needed direction, i.e. no simultaneous
    // accidental swipes in both directions
    if (otherDirection != QSwipeGesture::NoDirection) {
        return;
    }

    if (!m_display->activeToolbar())
        return;

    if (currentDirection == prevDirection) {
        m_display->setPrevActiveToolbar();
    }
    else if (currentDirection == nextDirection) {
        m_display->setNextActiveToolbar();
    }
    m_toolbarsComboBox->setCurrentIndex(m_display->activeToolbarIndex());
    showBanner(QString("Active toolbar is now '%1'"
                   ).arg(m_display->activeToolbar()->name));
}

void MTermWidget::pinchTriggered(QPinchGesture *gesture)
{
    QPinchGesture::ChangeFlags changeFlags = gesture->changeFlags();

    // initially scaleFactor is 1.0
    static const qreal scaleFactorInit = 1.0;
    static qreal lastTotalScaleFactor = scaleFactorInit;
        
    if (changeFlags & QPinchGesture::ScaleFactorChanged) {
        qreal totalScaleFactor = gesture->totalScaleFactor();
        qreal diff = totalScaleFactor - lastTotalScaleFactor;

        if (diff > .7) {
            m_terminalDisplay->increaseTextSize();
            lastTotalScaleFactor = totalScaleFactor;
        }
        if (diff < -.3) {
            m_terminalDisplay->decreaseTextSize();
            lastTotalScaleFactor = totalScaleFactor;
        }
    }
    if (gesture->state() == Qt::GestureFinished) {
        lastTotalScaleFactor = scaleFactorInit;
    }
}

void MTermWidget::displayFontChanged() const
{
    QFont font = m_terminalDisplay->getVTFont();

    showBanner(QString("Font size is now %1 ").arg(font.pointSize()));
}

void MTermWidget::showBanner(const QString &title) const
{
    MBanner *banner = new MBanner();
    // InformationBanner takes up too much space
    // banner->setStyleName("InformationBanner"); 
    banner->setStyleName("ShortEventBanner");
    banner->setTitle(title);
    banner->appear(scene(), MSceneWindow::DestroyWhenDone);
}

void MTermWidget::readSettings()
{
    QSettings settings("Nokia", APP_NAME_SETTINGS);

    QFont font;
    QFont defaultFont("Monospace", 12);
    font.fromString(settings.value("font", defaultFont.toString()).toString());
    m_colorScheme = settings.value("colorScheme",
                                   COLOR_SCHEME_GREEN_ON_BLACK).toInt();
    bool fullScreen = settings.value("fullScreen", true).toBool();
    QString activeToolbar =
        settings.value("activeToolbar",
                       QString("%1%2").arg(IM_TOOLBARS_DIR).arg("shell.xml")
                       ).toString();

    setTerminalFont(font);
    setColorScheme(m_colorScheme);
    toggleFullScreenMode(fullScreen);
    m_toolbarsComboBox->setCurrentIndex(
        m_display->setActiveToolbar(activeToolbar));
}

void MTermWidget::writeSettings() const
{
    QSettings settings("Nokia", APP_NAME_SETTINGS);

    settings.setValue("font", m_terminalDisplay->getVTFont().toString());
    settings.setValue("colorScheme", m_colorScheme);
    settings.setValue("fullScreen", m_fullScreenAction->isChecked());
    if (m_display->activeToolbar())
        settings.setValue("activeToolbar", m_display->activeToolbar()->fileName);
}

/**
 * [1] Background color is set to the one in the scheme of the terminal
 * display. This is to avoid flicker when display is resized due to vkb
 * show/hide events. The display is resized in resizeEvent, but vkb is still
 * animating, though it reports full size, so for a while white default
 * background of this widget becomes visible: hence the flicker.
 */
void MTermWidget::setColorScheme(int scheme)
{
    QGraphicsTermWidget::setColorScheme(scheme);
    m_colorScheme = scheme;

    QColor bgColor = m_terminalDisplay->colorTable()[DEFAULT_BACK_COLOR].color;

    QPalette p = palette(); // see [1]
    p.setColor(QPalette::Window, bgColor);
    setPalette(p);

    // set color of UI widgets opposite of the color scheme background
    // FIXME: check for actual bkg color instead of schemes
    QString piStyle;
    // bright background
    if (m_colorScheme == COLOR_SCHEME_BLACK_ON_LIGHT_YELLOW) {
        piStyle = "CommonPositionIndicator";
        m_selectionButton->setIconID("icon-m-toolbar-callhistory");
        m_selectionButton->setToggledIconID("icon-m-toolbar-trim");
        m_menuButton->setIconID("icon-m-toolbar-view-menu");
    }
    else {  // dark background
        piStyle = "CommonPositionIndicatorInverted";
        m_selectionButton->setIconID("icon-m-toolbar-callhistory-white-selected");
        m_selectionButton->setToggledIconID("icon-m-toolbar-trim-white-selected");
        m_menuButton->setIconID("icon-m-toolbar-view-menu-white-selected");
    }
    if (piStyle != m_viewport->positionIndicator()->styleName())
        m_viewport->positionIndicator()->setStyleName(piStyle);

    // update if changed not by combo box, i.e. by swiping
    if (m_colorSchemeComboBox->currentIndex() != scheme - 1)
        m_colorSchemeComboBox->setCurrentIndex(scheme -1);
}

/**
 * Overridden to check if the session changes the program in its run method. If
 * it does, then the user has specified invalid program name with the -e option
 * and the session has changed it to a shell program. In that case, shows a
 * message box explaining the case.
 */
void MTermWidget::startShellProgram()
{
    if ( m_session->isRunning() )
        return;

    QString oldProgram = m_session->program();
    QGraphicsTermWidget::startShellProgram();
    QString newProgram = m_session->program();

    if (oldProgram != newProgram) {
        QString title = QString("Warning!");
        QString text = QString("Could not find '%1', starting '%2' instead."
                               ).arg(oldProgram).arg(newProgram);

        MMessageBox *messageBox = new MMessageBox(title, text);
        messageBox->setIconId("icon-l-error");
        messageBox->appear(MSceneWindow::DestroyWhenDone);
    }
}

void MTermWidget::toggleFullScreenMode(bool fullScreen)
{
    MWindow *window = MApplication::activeWindow();
    Q_ASSERT(window);

    m_fullScreenAction->setChecked(fullScreen);

    if (fullScreen) {
        window->showFullScreen();
        m_fullScreenAction->setText("Disable FullScreen");
    } else {
        window->showNormal();
        m_fullScreenAction->setText("Enable FullScreen");
    }
}

void MTermWidget::onColorSchemeCbxCurrentIndexChanged(int index)
{
    setColorScheme(index + 1);
}

void MTermWidget::createNewWindow() const
{
    static QString myProgName = QApplication::arguments()[0];

    bool result = QProcess::startDetached(myProgName, QStringList() << "-n");
    if (!result)
        qDebug("Failed creating new window!");
}

void MTermWidget::showHelp()
{
    QString text = QString(
        "Swipe down on the virtual keyboard to hide it.<br><br>"
        "Semi-transparent button in the top-right corner opens the menu. "
        "The button under it toggles the text selection mode.<br><br>"
        "The text selection mode enables you to select and copy "
        "text. Double-tap to select a word, triple-tap to select an entire "
        "line. To enable column/block selection mode press Ctrl+Alt keys "
        "before commencing the selection. Copied text remains in the "
        "clipboard, so it can be pasted multiple times. Use the 'Clear' "
        "button to empty the clipboard contents.<br><br>"
        "While the text selection mode is disabled these gestures are "
        "supported by the terminal window (not the virtual keyboard):<br><br>"
        "Pinch to change font size.<br><br>"
        "Swipe horizontally to change toolbar.<br><br>"
        "Toolbars are specified in XML and are located in %1. Number of "
        "supported toolbars is unlimited, so you can add your own toolbars "
        "to that directory. For the toolbar XML format specification see "
        "<a href='%2'>%2</a>"
        ).arg(IM_TOOLBARS_DIR).arg("http://apidocs.meego.com/1.1/platform/html/meego-im-framework/toolbarxml.html");

    MLabel *label = new MLabel(text, this);
    label->setStyleName("CommonBodyTextInverted");
    label->setAlignment(Qt::AlignCenter);
    label->setWordWrap(true);
    label->setWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);
    connect(label, SIGNAL(linkActivated(QString)), this, SLOT(openLink(QString)));

    MDialog *dialog = new MDialog(APP_NAME + " Help", M::NoStandardButton);
    dialog->setCentralWidget(label);
    dialog->setTitleBarIconId("icon-l-terminal");
    dialog->appear(MSceneWindow::DestroyWhenDone);
}

void MTermWidget::openLink(const QString& link) const
{
    QDesktopServices::openUrl(QUrl(link));
}

void MTermWidget::enableGestures()
{
    grabGesture(Qt::SwipeGesture);
    grabGesture(Qt::PinchGesture);
}

void MTermWidget::disableGestures()
{
    ungrabGesture(Qt::SwipeGesture);
    ungrabGesture(Qt::PinchGesture);
}

/**
 * If selection mode is enabled viewport panning and gestures are disabled.
 * That is because they interfere with selections mouse move events.
 */
void MTermWidget::toggleSelectionMode(bool selection)
{
    m_selectionButton->setChecked(selection);

    if (selection) {
        m_viewport->ungrabGesture(Qt::PanGesture);
        disableGestures();
        m_display->enableSelectionMode();
    }
    else {
        m_viewport->grabGestureWithCancelPolicy(
            Qt::PanGesture, Qt::GestureFlags(),
            MWidget::MouseEventCancelOnGestureStarted);

        enableGestures();
        m_display->disableSelectionMode();
    }
}

void MTermWidget::toggleSelectionModeWithBanner(bool selection)
{
    toggleSelectionMode(selection);
    showBanner(QString("Text selection mode is now %1"
                       ).arg(selection ? "enabled" : "disabled"));
}
