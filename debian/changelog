meego-terminal (0.2.2) unstable; urgency=low

  * New text selection mode with copy, paste and clear clipboard actions
    Fixes: NB#290847 - No way to copy/paste text in Terminal application

  * Rearranged shell toolbar so that all buttons fully fit in portrait mode
    Fixes: NB#280882 - Only half of the toolbar is visible in portrait

  * Removed ImhUrlCharactersOnly input method hint to have same VKB layout
    as most of application

  * Fixed the issue: in landscape, if fullscreen mode is disabled and
    hwkb is in use the last two lines of text hide behind IM toolbar

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Mon, 21 Nov 2011 15:13:46 +0200

meego-terminal (0.2.1) unstable; urgency=low

  * Set focus policy to ClickFocus due to a bug in Qt 4.7.4
  Fixes: NB#279876 - Terminal looses the input focus

  * QFont is save/read from settings, before was only font size

  * Fixed a bug of terminal crashing if there are no toolbars

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Fri, 02 Sep 2011 10:44:52 +0300

meego-terminal (0.2.0) unstable; urgency=low

  * Fixed white on black color scheme to render bolded text

  * Start login shell to read commands from profile files
    Fixes: NB#273336 - Shell started by terminal does not read commands from
    profile files

  * Setting environment variable TERM=xterm

  * Set WINDOWID environment variable to window ID of view or its parent

  * Implemented semi-transparent menu button in the top-left corner and
    several menu actions

  * Added support for multiple toolbars, horizontal swipe changes toolbar.
    Two toolbars included by default: shell and arrows.

  * Implemented latching and locking of toolbar modifier buttons

  * Added support for Alt modifier in toolbars

  * Removed aegis manifest and GPG signature
    Fixes: NB#278648 - Remove aegis manifest and random signature from
    meego-terminal

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Thu, 29 Aug 2011 11:18:06 +0300

meego-terminal (0.1.8.1) unstable; urgency=low

  * Icon updated
    Fixes: NB#269397 - hard coded terminal icon icon for launcher is used

  * Updated maintainer field in debian/control
    Fixes: NB#271654 - Update maintainer field to a generic email address

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Tue, 12 Jul 2011 16:27:18 +0300

meego-terminal (0.1.8) unstable; urgency=low

   * Modified translator so that Return+Shift results in a newline
     Fixes: NB#247859 - The return key in VKB does not work in Caps Lock mode

   * If invalid command specified with -e show error message and start shell
     Fixes: NB#259767 - Terminal should give warning if program given with
     -e is missing and do something sensible

   * Added option -n that creates new instance

   * Replaced comma with forward slash in default VKB view

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Tue, 21 Jun 2011 15:14:50 +0300

meego-terminal (0.1.7) unstable; urgency=low

  * Added to the debian package aegis manifest and PGP signature
    Fixes: NB#253722 - meego-terminal package should have a manifest and
    be signed with a random PGP key

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Wed, 08 Jun 2011 13:28:52 +0300

meego-terminal (0.1.6) unstable; urgency=low

  * Setting widget background color according to the scheme
    Fixes: NB#246914 - White flicker before the VKB opens

  * Using white position indicator if background is black
    Fixes: NB#252754 - Terminal should have white scrollbar

  * Updated icon
    Fixes: NB#259482 - Update icon for meego-terminal application

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Wed, 25 May 2011 15:47:31 +0300

meego-terminal (0.1.5) unstable; urgency=low

  * Corrected calculation of the size when vkb is shown
    Fixes: NB#251812 - VKB hides part of the terminal in portrait mode

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Tue, 10 May 2011 14:53:01 +0300

meego-terminal (0.1.4) unstable; urgency=low

  * Made Esc button on the toolbar work
    Fixes: NB#247674 - Esc button in the IM toolbar does not work

  * New feature: pinch to change font size

  * New experimental feature: swipe horizontally to change color scheme

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Mon, 02 May 2011 15:04:44 +0300

meego-terminal (0.1.3) unstable; urgency=low

  * Added support for -e command line option.
    Fixes: NB#245116 - Option to specify a program to run with the terminal

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Thu, 14 Apr 2011 16:07:49 +0300

meego-terminal (0.1.2) unstable; urgency=low

  * Added a toolbar with keys missing or well hidden in the hwkb or the vkb.
    Fixes: NB#242486 - Add toolbar to terminal with keys missing or well
    hidden in the virtual keyboard or the hardware keyboard
    Fixes: NB#242807 - No control key in console virtual keyboard

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Fri, 01 Apr 2011 10:40:32 +0300

meego-terminal (0.1.1) unstable; urgency=low

  * Added support for virtual keyboard
    Fixes: NB#231185 - [FEA] Shell access on device
    Fixes: NB#210660 - Virtual keyboard is not opened in Qonsole

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Tue, 15 Mar 2011 10:24:15 +0200

meego-terminal (0.1.0) unstable; urgency=low

  * Initial release

 -- Ruslan Mstoi <ruslan.mstoi@nokia.com>  Wed, 02 Mar 2011 15:20:53 +0200
